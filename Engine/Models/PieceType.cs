﻿namespace Engine.Models
{
    public enum PieceType
    {
        Pawn,
        Rook,
        Night,
        Bishop,
        Queen,
        King
    }
}
