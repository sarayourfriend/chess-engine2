﻿using System.Collections.Generic;

namespace Engine.Models
{
    public class Queen : Piece
    {
        public Queen(int x, int y, PieceColor pieceColor) : base(x, y, PieceType.Queen, pieceColor)
        {

        }

        public override List<Move> GetMoves(Cell[,] cells, bool top = false)
        {
            var moves = new List<Move>();

            #region Upward iteration
            for (int y = 1; y <= Y; y++)
            {
                if (cells[X, Y - y].Piece == null)
                {
                    moves.Add(new Move(X, Y - y, this));
                }
                else if (cells[X, Y - y].Piece.Color != Color)
                {
                    moves.Add(new Move(X, Y - y, this));
                    break;
                }
                else
                {
                    break;
                }
            }
            #endregion

            #region Downward iteration
            for (int y = 1; y <= 7 - Y; y++)
            {
                if (cells[X, Y + y].Piece == null)
                {
                    moves.Add(new Move(X, Y + y, this));
                }
                else if (cells[X, Y + y].Piece.Color != Color)
                {
                    moves.Add(new Move(X, Y + y, this));
                    break;
                }
                else
                {
                    break;
                }
            }
            #endregion

            #region Leftward iteration
            for (int x = 1; x <= X; x++)
            {
                if (cells[X - x, Y].Piece == null)
                {
                    moves.Add(new Move(X - x, Y, this));
                }
                else if (cells[X - x, Y].Piece.Color != Color)
                {
                    moves.Add(new Move(X - x, Y, this));
                    break;
                }
                else
                {
                    break;
                }
            }
            #endregion

            #region Rightward iteration
            for (int x = 1; x <= 7 - X; x++)
            {
                if (cells[X + x, Y].Piece == null)
                {
                    moves.Add(new Move(X + x, Y, this));
                }
                else if (cells[X + x, Y].Piece.Color != Color)
                {
                    moves.Add(new Move(X + x, Y, this));
                    break;
                }
                else
                {
                    break;
                }
            }
            #endregion

            #region Up left iteration
            if (X <= Y)
            {
                for (int x = 1; x <= X; x++)
                {
                    if (cells[X - x, Y - x].Piece == null)
                    {
                        moves.Add(new Move(X - x, Y - x, this));
                    }
                    else if (cells[X - x, Y - x].Piece.Color != Color)
                    {
                        moves.Add(new Move(X - x, Y - x, this));
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            else
            {
                for (int y = 1; y <= Y; y++)
                {
                    if (cells[X - y, Y - y].Piece == null)
                    {
                        moves.Add(new Move(X - y, Y - y, this));
                    }
                    else if (cells[X - y, Y - y].Piece.Color != Color)
                    {
                        moves.Add(new Move(X - y, Y - y, this));
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            #endregion

            #region Up right iteration
            if (X <= 7 - Y)
            {
                for (int y = 1; y <= Y; y++)
                {
                    if (cells[X + y, Y - y].Piece == null)
                    {
                        moves.Add(new Move(X + y, Y - y, this));
                    }
                    else if (cells[X + y, Y - y].Piece.Color != Color)
                    {
                        moves.Add(new Move(X + y, Y - y, this));
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            else
            {
                for (int x = 1; x <= 7 - X; x++)
                {
                    if (cells[X + x, Y - x].Piece == null)
                    {
                        moves.Add(new Move(X + x, Y - x, this));
                    }
                    else if (cells[X + x, Y - x].Piece.Color != Color)
                    {
                        moves.Add(new Move(X + x, Y - x, this));
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            #endregion

            #region Down right iteration
            if (X >= Y)
            {
                for (int x = 1; x <= 7 - X; x++)
                {
                    if (cells[X + x, Y + x].Piece == null)
                    {
                        moves.Add(new Move(X + x, Y + x, this));
                    }
                    else if (cells[X + x, Y + x].Piece.Color != Color)
                    {
                        moves.Add(new Move(X + x, Y + x, this));
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            else
            {
                for (int y = 1; y <= 7 - Y; y++)
                {
                    if (cells[X + y, Y + y].Piece == null)
                    {
                        moves.Add(new Move(X + y, Y + y, this));
                    }
                    else if (cells[X + y, Y + y].Piece.Color != Color)
                    {
                        moves.Add(new Move(X + y, Y + y, this));
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            #endregion

            #region Down left iteration
            if (X <= 7 - Y)
            {
                for (int x = 1; x <= X; x++)
                {
                    if (cells[X - x, Y + x].Piece == null)
                    {
                        moves.Add(new Move(X - x, Y + x, this));
                    }
                    else if (cells[X - x, Y + x].Piece.Color != Color)
                    {
                        moves.Add(new Move(X - x, Y + x, this));
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            else
            {
                for (int y = 1; y <= 7 - Y; y++)
                {
                    if (cells[X - y, Y + y].Piece == null)
                    {
                        moves.Add(new Move(X - y, Y + y, this));
                    }
                    else if (cells[X - y, Y + y].Piece.Color != Color)
                    {
                        moves.Add(new Move(X - y, Y + y, this));
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            #endregion

            return moves;
        }
    }
}
