﻿using System.Collections.Generic;
using System.Linq;

namespace Engine.Models
{
    public class King : Piece
    {
        public King(int x, int y, PieceColor pieceColor) : base(x, y, PieceType.King, pieceColor)
        {

        }

        public override List<Move> GetMoves(Cell[,] cells, bool top = false)
        {
            var uncheckedMoves = UncheckedMoves(cells);

            if (top == false)
            {
                return uncheckedMoves;
            }

            var check = CheckOrCheckMate(cells);

            foreach (var move in uncheckedMoves)
            {
                if (!check.Exists(m => m.ToX == move.ToX && m.ToY == move.ToY))
                {
                    check.Insert(0, new Move(-1, -1, this));
                }
            }

            return uncheckedMoves;
        }

        public List<Move> UncheckedMoves(Cell[,] cells)
        {
            var moves = new List<Move>();

            if (Y > 0)
            {
                if (cells[X, Y - 1].Piece == null ||
                    cells[X, Y - 1].Piece.Color != Color)
                {
                    moves.Add(new Move(X, Y - 1, this));
                }
            }

            if (Y < 7)
            {
                if (cells[X, Y + 1].Piece == null ||
                    cells[X, Y + 1].Piece.Color != Color)
                {
                    moves.Add(new Move(X, Y + 1, this));
                }
            }

            if (X > 0)
            {
                if (cells[X - 1, Y].Piece == null ||
                    cells[X - 1, Y].Piece.Color != Color)
                {
                    moves.Add(new Move(X - 1, Y, this));
                }
            }

            if (X < 7)
            {
                if (cells[X + 1, Y].Piece == null ||
                    cells[X + 1, Y].Piece.Color != Color)
                {
                    moves.Add(new Move(X + 1, Y, this));
                }
            }

            if (X > 0 && Y > 0)
            {
                if (cells[X - 1, Y - 1].Piece == null ||
                    cells[X - 1, Y - 1].Piece.Color != Color)
                {
                    moves.Add(new Move(X - 1, Y - 1, this));
                }
            }

            if (X > 0 && Y < 7)
            {
                if (cells[X - 1, Y + 1].Piece == null ||
                    cells[X - 1, Y + 1].Piece.Color != Color)
                {
                    moves.Add(new Move(X - 1, Y + 1, this));
                }
            }

            if (X < 7 && Y > 0)
            {
                if (cells[X + 1, Y - 1].Piece == null ||
                    cells[X + 1, Y - 1].Piece.Color != Color)
                {
                    moves.Add(new Move(X + 1, Y - 1, this));
                }
            }

            if (X < 7 && Y < 7)
            {
                if (cells[X + 1, Y + 1].Piece == null ||
                    cells[X + 1, Y + 1].Piece.Color != Color)
                {
                    moves.Add(new Move(X + 1, Y + 1, this));
                }
            }

            return moves;
        }

        public List<Move> CheckOrCheckMate(Cell[,] cells)
        {
            var attackingMoves = new List<Move>();
            var directAttacks = new List<Move>();

            foreach (var cell in cells)
            {
                if (cell.Piece != null && cell.Piece.Color != Color)
                {
                    var moves = cell.Piece.GetMoves(cells);

                    attackingMoves.AddRange(moves);
                    directAttacks.AddRange(moves.Where(m => m.ToX == X && m.ToY == Y));
                }
            }

            var kingMoves = this.UncheckedMoves(cells);
            kingMoves.RemoveAll(m => attackingMoves.Exists(a => a.ToX == m.ToX && a.ToY == m.ToY));

            var illegalKingMoves = new List<Move>();

            foreach (var move in kingMoves)
            {
                if (cells[move.ToX, move.ToY].Piece != null &&
                    cells[move.ToX, move.ToY].Piece.Color != Color)
                {
                    var holding = cells[move.ToX, move.ToY].Piece;
                    cells[holding.X, holding.Y].Piece = this;

                    var afterMoves = new List<Move>();

                    foreach (var cell in cells)
                    {
                        if (cell.Piece != null &&
                            cell.Piece.Color != Color)
                        {
                            var moves = cell.Piece.GetMoves(cells);

                            if (moves.Exists(m => m.ToX == holding.X && m.ToY == holding.Y))
                            {
                                illegalKingMoves.Add(move);
                            }
                        }
                    }
                    cells[X, Y].Piece = this;
                    cells[holding.X, holding.Y].Piece = holding;
                }
            }

            if (illegalKingMoves.Count != 0)
            {
                kingMoves.RemoveAll(m => illegalKingMoves.Exists(i => i.ToX == m.ToX && i.ToY == m.ToY));
            }

            if (kingMoves.Count != 0)
            {
                return kingMoves;
            }

            var teamMoves = new List<Move>();

            // Gather all opposite color pieces' moves
            foreach (var cell in cells)
            {
                if (cell.Piece != null &&
                    cell.Piece.Color == Color &&
                    cell.Piece.Type != PieceType.King)
                {
                    teamMoves.AddRange(cell.Piece.GetMoves(cells));
                }
            }

            // Teammates can't move, king can't move
            // CHECK MATE
            if (teamMoves.Count == 0)
            {
                return new List<Move>() { new Move(X, Y, this) };
            }

            var defensiveMoves = teamMoves.Where(m => attackingMoves.Exists(a => a.Piece.X == m.ToX && a.Piece.Y == m.ToY) ||
                                                      attackingMoves.Exists(a => IsMoveBetweenKingAndAttacker(m, a.Piece)));

            if (defensiveMoves.Count() == 0)
            {
                // No one can help the poor king. Check mate!
                return new List<Move>() { new Move(X, Y, this) };
            }

            var mobileDefenders = new List<Move>();

            foreach (var move in defensiveMoves)
            {
                var offBoardHold = move.Piece;

                cells[offBoardHold.X, offBoardHold.Y].Piece = null;

                var afterMoves = new List<Move>();

                foreach (var cell in cells)
                {
                    if (cell.Piece != null &&
                        cell.Piece.Color != Color)
                    {
                        if (!(cell.Piece.GetMoves(cells).Exists(m => m.ToX == X && m.ToY == Y)))
                        {
                            afterMoves.AddRange(cell.Piece.GetMoves(cells).Where(m => m.ToX == X && m.ToY == Y));
                        }
                    }
                }

                if (afterMoves.Count == 0)
                {
                    mobileDefenders.Add(move);
                }

                cells[offBoardHold.X, offBoardHold.Y].Piece = offBoardHold;
                offBoardHold = null;
            }

            if (mobileDefenders.Count > 0)
            {
                return mobileDefenders;
            }

            // This means there aren't any pieces that can
            // move between the king and the attacker so...
            return new List<Move>() { new Move(X, Y, this) };
        }

        public bool IsMoveBetweenKingAndAttacker(Move move, Piece attacker)
        {
            if ((move.ToX < attacker.X && move.ToX > X &&
                 move.ToY < attacker.Y && move.ToY > Y)
                 ||
                (move.ToX > attacker.X && move.ToX < X &&
                 move.ToY < attacker.Y && move.ToY > Y)
                 ||
                (move.ToX < attacker.X && move.ToX > X &&
                 move.ToY > attacker.Y && move.ToY < Y)
                 ||
                (move.ToX > attacker.X && move.ToX < X &&
                 move.ToY > attacker.Y && move.ToY < Y)
                 ||
                (move.ToX == attacker.X &&
                 move.ToY > attacker.Y && move.ToY < Y)
                 ||
                (move.ToX == attacker.X &&
                 move.ToY < attacker.Y && move.ToY > Y)
                 ||
                (move.ToY == attacker.Y &&
                 move.ToX > attacker.X && move.ToX < X)
                 ||
                (move.ToY == attacker.Y &&
                 move.ToX < attacker.X && move.ToX > X))
            {
                return true;
            }
            return false;
        }
    }
}
