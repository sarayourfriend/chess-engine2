﻿namespace Engine.Models
{
    public class Move
    {
        public int ToX { get; set; }
        public int ToY { get; set; }
        public Piece Piece { get; set; }

        public Move(int toX, int toY, Piece piece)
        {
            ToX = toX;
            ToY = toY;
            Piece = piece;
        }
    }
}
