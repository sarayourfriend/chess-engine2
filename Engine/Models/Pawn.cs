﻿using System.Collections.Generic;

namespace Engine.Models
{
    public class Pawn : Piece
    {
        public Pawn(int x, int y, PieceColor pieceColor) : base(x, y, PieceType.Pawn, pieceColor)
        {

        }

        public override List<Move> GetMoves(Cell[,] cells, bool top = false)
        {
            var moves = new List<Move>();

            if (Color == PieceColor.Dark)
            {
                if (X < 7 && Y < 7)
                {
                    if (cells[X + 1, Y + 1].Piece != null &&
                        cells[X + 1, Y + 1].Piece.Color == PieceColor.Light)
                    {
                        moves.Add(new Move(X + 1, Y + 1, this));
                    }
                }

                if (X > 0 && Y < 7)
                {
                    if (cells[X - 1, Y + 1].Piece != null &&
                        cells[X - 1, Y + 1].Piece.Color == PieceColor.Light)
                    {
                        moves.Add(new Move(X - 1, Y + 1, this));
                    }
                }

                if (!HasMoved)
                {
                    if (cells[X, Y + 2].Piece == null &&
                        cells[X, Y + 1].Piece == null)
                    {
                        moves.Add(new Move(X, Y + 2, this));
                    }
                }

                if (Y < 7 &&
                    cells[X, Y + 1].Piece == null)
                {
                    moves.Add(new Move(X, Y + 1, this));
                }

                return moves;
            }
            else
            {
                if (Y == 0)
                {
                    moves.Add(new Move(X, Y, this));
                }

                if (X < 7 && Y > 0)
                {
                    if (cells[X + 1, Y - 1].Piece != null &&
                        cells[X + 1, Y - 1].Piece.Color == PieceColor.Dark)
                    {
                        moves.Add(new Move(X + 1, Y - 1, this));
                    }
                }

                if (X > 0 && Y > 0)
                {
                    if (cells[X - 1, Y - 1].Piece != null &&
                        cells[X - 1, Y - 1].Piece.Color == PieceColor.Dark)
                    {
                        moves.Add(new Move(X - 1, Y - 1, this));
                    }
                }

                if (!HasMoved)
                {
                    if (cells[X, Y - 2].Piece == null &&
                        cells[X, Y - 1].Piece == null)
                    {
                        moves.Add(new Move(X, Y - 2, this));
                    }
                }

                if (Y > 0 &&
                    cells[X, Y - 1].Piece == null)
                {
                    moves.Add(new Move(X, Y - 1, this));
                }

                return moves;
            }
        }
    }
}
