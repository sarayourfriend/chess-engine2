﻿using System.Collections.Generic;

namespace Engine.Models
{
    public class Night : Piece
    {
        public Night(int x, int y, PieceColor pieceColor) : base(x, y, PieceType.Night, pieceColor)
        {

        }

        public override List<Move> GetMoves(Cell[,] cells, bool top = false)
        {
            var moves = new List<Move>();

            if (X - 2 > -1 && Y - 1 > -1)
            {
                if (cells[X - 2, Y - 1].Piece == null ||
                    cells[X - 2, Y - 1].Piece.Color != Color)
                {
                    moves.Add(new Move(X - 2, Y - 1, this));
                }
            }

            if (X - 2 > -1 && Y + 1 < 8)
            {
                if (cells[X - 2, Y + 1].Piece == null ||
                    cells[X - 2, Y + 1].Piece.Color != Color)
                {
                    moves.Add(new Move(X - 2, Y + 1, this));
                }
            }

            if (X - 1 > -1 && Y - 2 > -1)
            {
                if (cells[X - 1, Y - 2].Piece == null ||
                    cells[X - 1, Y - 2].Piece.Color != Color)
                {
                    moves.Add(new Move(X - 1, Y - 2, this));
                }
            }

            if (X - 1 > -1 && Y + 2 < 8)
            {
                if (cells[X - 1, Y + 2].Piece == null ||
                    cells[X - 1, Y + 2].Piece.Color != Color)
                {
                    moves.Add(new Move(X - 1, Y + 2, this));
                }
            }

            if (X + 1 < 8 && Y - 2 > -1)
            {
                if (cells[X + 1, Y - 2].Piece == null ||
                    cells[X + 1, Y - 2].Piece.Color != Color)
                {
                    moves.Add(new Move(X + 1, Y - 2, this));
                }
            }

            if (X + 1 < 8 && Y + 2 < 8)
            {
                if (cells[X + 1, Y + 2].Piece == null ||
                    cells[X + 1, Y + 2].Piece.Color != Color)
                {
                    moves.Add(new Move(X + 1, Y + 2, this));
                }
            }

            if (X + 2 < 8 && Y - 1 > -1)
            {
                if (cells[X + 2, Y - 1].Piece == null ||
                    cells[X + 2, Y - 1].Piece.Color != Color)
                {
                    moves.Add(new Move(X + 2, Y - 1, this));
                }
            }

            if (X + 2 < 8 && Y + 1 < 8)
            {
                if (cells[X + 2, Y + 1].Piece == null ||
                    cells[X + 2, Y + 1].Piece.Color != Color)
                {
                    moves.Add(new Move(X + 2, Y + 1, this));
                }
            }

            return moves;
        }
    }
}
