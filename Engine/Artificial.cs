﻿using Engine.Models;
using System.Collections.Generic;

namespace Engine
{
    public class Artificial
    {
        public Node highest;
        public int highestPoints;
        public Artificial(Board board, PieceColor color)
        {
            var moves = board.AllMoves(color);
            var nodes = new Node[moves.Count];
            for (int i = 0; i < nodes.Length; i++)
            {
                var node = nodes[i] = new Node(board, new List<Move>() { moves[i] }, 1, 0);
                node.BranchNode(0, 5);
            }

            this.highest = null;
            this.highestPoints = 0;
            Highest(nodes, this);
        }

        public static void Highest(Node[] nodes, Artificial root)
        {
            foreach (var node in nodes)
            {
                if (node.Points > root.highestPoints)
                {
                    root.highestPoints = node.Points;
                    root.highest = node;
                }

                if (node.Next != null)
                {
                    Highest(node.Next, root);
                }
            }
        }
    }
}
