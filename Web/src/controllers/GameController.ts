import { merge, copy } from 'angular';

import GameService from '../services/GameService';
import PieceFactory from '../services/PieceFactory';

import Game from '../models/games/Game';
import History from '../models/games/History';
import Move from '../models/games/Move';
import MoveValidation from '../models/games/MoveValidation';
import Piece from '../models/games/Piece';
import WinRecord from '../models/games/WinRecord';

import { BoardPromise } from '../models/routes/api/game/Board';
import { IGameRouteParams } from '../models/routes/browse/Game';
import { TURN_RESPONSE } from '../models/routes/api/game/Turn';
import { MoveValidationPromise, VALIDATION_RESPONSE } from '../models/routes/api/game/MoveValidation';

const AUDIO_ELEMENT_ID = 'bell';
const AUDIO_SOURCE = '/sounds/bell.mp3';

export default class GameController {
    static $inject = ['$interval', '$stateParams', 'GameService', 'BoardPromise'];
    private interval: ng.IIntervalService;
    private stateParams: IGameRouteParams;
    private gameService: GameService;
    private piecesSwap: Piece[];
    private audio: HTMLAudioElement;
    public audioOn: boolean;
    public currentTurn: string;
    public dragged: Piece;
    public draggedElement: ng.IAugmentedJQuery;
    public game: Game;
    public history: History;
    public historyToggle: boolean;
    public hovered: Piece;
    public moves: Move[] = [];
    public pieces: Piece[];
    public winRecord: WinRecord;

    public constructor(
        private $interval: ng.IIntervalService,
        private $stateParams: IGameRouteParams,
        private GameService: GameService,
        private BoardPromise: ng.IHttpPromiseCallbackArg<Piece[]>
    ) {
        this.interval = $interval;
        this.stateParams = $stateParams;
        this.gameService = GameService;
        this.pieces = BoardPromise.data;

        this.audioOn = true;
        this.audio = <HTMLAudioElement>document.getElementById(AUDIO_ELEMENT_ID);
        this.audio.src = AUDIO_SOURCE;
        this.winRecord = null;
        this.historyToggle = false;

        this.resolveName();
        this.resolveHistory();
        this.resolveMoves();
        this.resolveTurnInitial();
    }

    private resolveName(): void {
        this.gameService.game(this.stateParams.name).then(
            (res) => {
                this.game = res.data;
            },
            (err) => {
                console.error(err.data);
            }
        );
    }

    private resolveHistory(): void {
        this.gameService.history(this.stateParams.name).then(
            (res) => {
                this.history = res.data;
            },
            (err) => {
                console.error(err.data);
            }
        );
    }

    private resolveMoves(): void {
        this.gameService.moves(this.stateParams.name).then(
            (res) => {
                this.moves = res.data;
            },
            (err) => {
                console.error(err.data);
            }
        );
    }

    private resolveTurnInitial(): void {
        this.gameService.turn(this.stateParams.name).then(
            (res) => {
                this.currentTurn = res.data;
                switch (this.currentTurn) {
                    case TURN_RESPONSE.DENY:
                        this.pollForTurnSwitch();
                        break;
                    case TURN_RESPONSE.GAMEOVER:
                        this.finishGame();
                        break;
                }
            },
            (err) => {
                console.error(err.data);
            }
        );
    }

    private resolveTurnDuring(interval: ng.IPromise<any>): void {
        this.gameService.turn(this.stateParams.name).then(
            (res) => {
                this.currentTurn = res.data;
                switch (this.currentTurn) {
                    case TURN_RESPONSE.CONFIRM:
                        if (this.audioOn)
                            this.audio.play();
                        
                        this.resetup(interval);
                        break;
                    case TURN_RESPONSE.GAMEOVER:
                        this.finishGame(interval);
                        break;
                }
            },
            (err) => {
                console.error(err);
            }
        );
    }

    private resolvePieces(): void {
        this.gameService.board(this.stateParams.name).then(
            (res) => {
                this.pieces = res.data;
            },
            (err) => {
                console.error(err);
            }
        );
    }

    private resolveWinner(): void {
        this.gameService.winner(this.stateParams.name).then(
            (res) => {
                this.winRecord = res.data;
            },
            (err) => {
                console.error(err);
            }
        );
    }

    public resolveConfirmMove(validationMessage: string): void {
        switch (validationMessage) {
            case VALIDATION_RESPONSE.CONFIRM:
                this.clean();
                this.pollForTurnSwitch();
                break;
            case VALIDATION_RESPONSE.CONFIRM_AS_WIN:
                this.clean();
                this.finishGame();
                break;
        }
    }

    public togglePreviousBoard(): void {
        if (this.historyToggle) {
            this.pieces = copy(this.piecesSwap);
        } else {
            this.piecesSwap = copy(this.pieces);
            this.pieces = copy(this.history.pieces);
        }
        this.historyToggle = !this.historyToggle;
    }

    public toggleAudio(): void {
        this.audioOn = !this.audioOn;
    }

    public getPieceMoves(piece: Piece): Move[] {
        if (this.moves)
            return this.moves.filter(
                (obj, i, arr) => {
                    if (obj.fromFile == piece.file && obj.fromRank == piece.rank)
                        return obj;
                }
            );
    }

    public isDropAllowed(file: number, rank: number, fromDragged: boolean = true): boolean {
        let piece: Piece;
        if (fromDragged)
            piece = this.dragged;
        else
            piece = this.hovered;
        
        if (this.historyToggle) 
            return false;
        
        if (!piece)
            return false;    
        
        let moves = this.getPieceMoves(piece);
        if (moves)
            for (let m = 0, mLen = moves.length; m < mLen; m++) {
                let move = moves[m];
                if (move.toFile == file && move.toRank == rank)
                    return true;    
            }

        return false;
    }

    public checkDrop(file: number, rank: number): MoveValidationPromise {
        return this.gameService.makeMove(this.stateParams.name, {
            fromFile: this.dragged.file,
            fromRank: this.dragged.rank,
            toFile: file,
            toRank: rank
        });
    }

    private clean(): void {
        this.currentTurn = TURN_RESPONSE.DENY;
        this.draggedElement = null;
        this.dragged = null;
        this.moves = [];

        this.resolvePieces();
        this.resolveHistory();
    }

    private pollForTurnSwitch(): void {
        let interval = this.$interval(
            () => this.resolveTurnDuring(interval), 5000
        );
    }

    private resetup(interval: ng.IPromise<any>): void {
        this.interval.cancel(interval);
        this.resolvePieces();
        this.resolveHistory();
        this.resolveMoves();
    }

    private finishGame(interval: ng.IPromise<any> = null): void {
        if (interval)
            this.interval.cancel(interval);
        
        this.moves = [];

        this.resolvePieces();
        this.resolveHistory();
        this.resolveWinner();
    }
}