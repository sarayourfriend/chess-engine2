import Piece from './Piece';

class HistoryMove {
    id: number;
    value: string;
}

export default class History {
    history: HistoryMove[];
    pieces: Piece[];
}
