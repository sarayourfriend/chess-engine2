export default class Move {
    fromFile: number;
    fromRank: number;
    toFile: number;
    toRank: number;
}
