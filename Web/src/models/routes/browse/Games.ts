import { ROOT_ROUTE } from './Root';

export class GamesRoute implements angular.ui.IState {
    public url: string = '/games';
    public controller: string = 'GameBrowserController';
    public controllerAs: string = 'gbc';
    public template: string = require('./templates/games.html');
}

export const GAMES_ROUTE = `${ROOT_ROUTE}.GAMES`;
