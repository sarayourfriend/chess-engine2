import { merge } from 'angular';

import Game from '../../games/Game';

import AuthenticationService from '../../../services/AuthenticationService';
import AuthenticationValue from '../../../services/AuthenticationValue';
import DateStringFactory from '../../../services/DateStringFactory';

import { AuthenticationPromise } from '../api/account/Authentication';

export class RootRoute implements angular.ui.IState {
    public abstract: boolean = true;
    public url: string = '';
    public controller: string = 'RootController';
    public controllerAs: string = 'rc';
    public template: string = require('./templates/root.html');
    public resolve: {} = {
        AuthenticationPromise: this.AuthenticationPromise()
    };

    private AuthenticationPromise(): any[] {
        return [
            'AuthenticationService',
            'AuthenticationValue',
            'DateStringFactory',
            this.resolveAuthenticationStatus
        ];
    }

    private resolveAuthenticationStatus(
        AuthenticationService: AuthenticationService,
        AuthenticationValue: AuthenticationValue,
        DateStringFactory: DateStringFactory
    ): AuthenticationPromise {
        let promise = AuthenticationService.status()
        promise.then(
            (res) => {
                let value: AuthenticationValue = {
                    id: '',
                    userName: '',
                    games: []
                };
                value.id = res.data.id;
                value.userName = res.data.userName;
                value.games = res.data.games.map(
                    (obj: Game, i: number, arr: Game[]) => {
                        let game: Game = new Game();
                        merge(game, obj);
                        game.startDate = DateStringFactory.create(game.startDate);
                        return game;
                    }
                )
                merge(AuthenticationValue, value);
            }
        );
        return promise;
    };
}

export const ROOT_ROUTE = 'ROOT';