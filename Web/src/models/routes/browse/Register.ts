import { ROOT_ROUTE } from './Root';

export class RegisterRoute implements angular.ui.IState {
    public url: string = '/register';
    public template: string = require('./templates/register.html');
}

export const REGISTER_ROUTE = `${ROOT_ROUTE}.REGISTER`;