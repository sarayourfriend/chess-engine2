export interface TurnPromise extends ng.IHttpPromise<string> { }

export const TURN_ROUTE = (name: string) => {
    return `/api/games/${name}/playerturn`;
};

export const TURN_RESPONSE = {
    DENY: 'DENY',
    CONFIRM: 'CONFIRM',
    GAMEOVER: 'GAMEOVER'
};
