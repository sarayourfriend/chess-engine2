import History from '../../../games/History';

export interface HistoryPromise extends ng.IHttpPromise<History> { }

export const HISTORY_ROUTE = (name: string) => {
    return `/api/games/${name}/history`;
};