import Game from '../../../games/Game';

export interface UnmatchedPromise extends ng.IHttpPromise<Game[]> { }

export const UNMATCHED_ROUTE = '/api/games/unmatched';
