import MoveValidation from '../../../games/MoveValidation';

export interface MoveValidationPromise extends ng.IHttpPromise<MoveValidation> { }

export const MOVE_VALIDATION_ROUTE = (name: string) => {
    return `/api/games/${name}/moves`;
};

export const VALIDATION_RESPONSE = {
    CONFIRM: 'CONFIRM',
    CONFIRM_AS_WIN: 'CONFIRM_AS_WIN',
    DENY: 'DENY'
};