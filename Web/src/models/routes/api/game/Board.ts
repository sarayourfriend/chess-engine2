import Piece from '../../../games/Piece';

export interface BoardPromise extends ng.IHttpPromise<Piece[]> { }

export const BOARD_ROUTE = (name: string) => {
    return `/api/games/${name}/board`;
};