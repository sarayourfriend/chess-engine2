import Game from '../../../games/Game';

export interface GamePromise extends ng.IHttpPromise<Game> { }

export const GAME_ROUTE = (name: string) => {
    return `/api/games/${name}`;
};
