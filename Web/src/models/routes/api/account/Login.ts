import LoginRequest from '../../../accounts/LoginRequest';

export interface LoginPromise extends ng.IHttpPromise<LoginRequest> { }

export const LOGIN_ROUTE = '/api/accounts/login';
