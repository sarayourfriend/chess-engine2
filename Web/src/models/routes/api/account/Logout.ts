import User from '../../../accounts/User';

export interface LogoutPromise extends ng.IHttpPromise<User> { }

export const LOGOUT_ROUTE = '/api/accounts/logout';
