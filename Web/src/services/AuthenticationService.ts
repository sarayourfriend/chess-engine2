import {
    AuthenticationPromise,
    AUTHENTICATION_ROUTE
} from '../models/routes/api/account/Authentication';
import {
    LogoutPromise,
    LOGOUT_ROUTE
} from '../models/routes/api/account/Logout';
import {
    LoginPromise,
    LOGIN_ROUTE
} from '../models/routes/api/account/Login';
import {
    RegisterPromise,
    REGISTER_ROUTE
} from '../models/routes/api/account/Register';
import LoginRequest from '../models/accounts/LoginRequest';
import RegisterRequest from '../models/accounts/RegisterRequest';

export default class AuthenticationService {
    static $inject = ['$http'];
    private http: ng.IHttpService;

    public constructor(
        private $http: ng.IHttpService
    ) {
        this.http = $http;
    }

    public status(): AuthenticationPromise {
        return this.http.get(AUTHENTICATION_ROUTE);
    }

    public register(registerRequest: RegisterRequest): RegisterPromise {
        return this.http.post(REGISTER_ROUTE, registerRequest);
    }

    public login(loginRequest: LoginRequest): LoginPromise {
        return this.http.post(LOGIN_ROUTE, loginRequest);
    }

    public logout(): AuthenticationPromise {
        return this.http.put(LOGOUT_ROUTE, null);
    }
}