export default class DateStringFactory {
    public create(date: string): string {
        let d = new Date(date);
        return `${d.toLocaleDateString()} ${d.toLocaleTimeString()}`;
    }
}