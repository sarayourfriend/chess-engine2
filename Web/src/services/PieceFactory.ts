import Piece from '../models/games/Piece';

export default class PieceFactory {
    public create(piece: Piece): string {
        let rank = piece.rank;
        let file = piece.file;
        let color = piece.color.toLowerCase();
        let type = piece.type.toLowerCase();
        return `<div ce-piece color="${color}" type="${type}" file="${file}" rank="${rank}" class="${color} ${type}" draggable="true"></div>`;
    }
}
