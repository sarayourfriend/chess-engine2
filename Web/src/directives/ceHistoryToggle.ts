class Link {
    private compile: ng.ICompileService;

    constructor(
        private scope: ng.IScope,
        private element: ng.IAugmentedJQuery,
        private attrs: ng.IAttributes,
        $compile: ng.ICompileService
    ) {
        this.compile = $compile;

        this.registerToggleWatch();
    }

    private registerToggleWatch(): void {
        this.scope.$watch('gc.historyToggle',
            (newValue: boolean, oldValue, scope) => {
                let template = this.makeTemplate(newValue);
                this.element.empty();
                this.element.append(this.compile(template)(this.scope));
            }
        )
    }

    private makeTemplate(on: boolean): string {
        let text: string;
        if (on)
            text = 'Current board';
        else
            text = 'Previous board';
        
        return `<button class="ce-btn primary" ng-click="gc.togglePreviousBoard()">${text}</button>`;
    }
}

export default function HistoryToggleDirective($compile: ng.ICompileService) {
    return {
        link: (scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes): Link => new Link(scope, element, attrs, $compile)
    };
}

HistoryToggleDirective.$inject = ['$compile'];
