class Link {
    private compile: ng.ICompileService;

    constructor(
        private scope: ng.IScope,
        private element: ng.IAugmentedJQuery,
        private attrs: ng.IAttributes,
        private $compile: ng.ICompileService
    ) {
        this.compile = $compile;

        this.registerToggleWatch();
    }

    private registerToggleWatch(): void {
        this.scope.$watch('gc.audioOn',
            (newValue: boolean, oldValue: boolean, scope) => {
                let template = this.makeTemplate(newValue);
                this.element.empty();
                this.element.append(this.compile(template)(this.scope));
            }
        )
    }

    private makeTemplate(on: boolean): string {
        let quality: string;
        if (on)
            quality = '-up';
        else
            quality = '-off';

        return `<i ng-click="gc.toggleAudio()" class="fa fa-volume${quality} fa-fw"></i>`;
            
    }
}

export default function AudioToggleDirective($compile: ng.ICompileService) {
    return {
        link: (scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes): Link => new Link(scope, element, attrs, $compile)
    }
}

AudioToggleDirective.$inject = ['$compile'];