import { TURN_RESPONSE } from '../models/routes/api/game/Turn';

class Link {
    private compile: ng.ICompileService;

    constructor(
        private scope: ng.IScope,
        private element: ng.IAugmentedJQuery,
        private attrs: ng.IAttributes,
        private $compile: ng.ICompileService
    ) {
        this.compile = $compile;

        this.registerTurnWatch();
    }

    private registerTurnWatch(): void {
        this.scope.$watch('gc.currentTurn',
            (newValue: string, oldValue, scope) => {
                let template = this.makeTemplate(newValue);
                this.element.empty();
                this.element.append(this.compile(template)(this.scope));
            }
        );
    }

    private makeTemplate(response: string): string {
        let text;
        switch (response) {
            case TURN_RESPONSE.CONFIRM:
                text = 'It\'s your turn';
                break;
            case TURN_RESPONSE.DENY:
                text = 'Waiting...';
                break;
            case TURN_RESPONSE.GAMEOVER:
                text = 'Game finished';
                break;
        }
        return `<div>${text}</div>`;
    }
}

export default function CurrentTurnDirective($compile: ng.ICompileService) {
    return {
        link: (scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes): Link => new Link(scope, element, attrs, $compile)
    };
}

CurrentTurnDirective.$inject = ['$compile'];
