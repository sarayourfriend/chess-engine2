import { merge } from 'angular';

import GameController from '../controllers/GameController';
import PieceFactory from '../services/PieceFactory';
import Piece from '../models/games/Piece';

import { VALIDATION_RESPONSE } from '../models/routes/api/game/MoveValidation';

interface ICellAttrs extends ng.IAttributes {
    file: string;
    rank: string;
}

interface ICellScope extends ng.IScope {
    gc: GameController;
}

class Link {
    private file: number;
    private rank: number;
    private compile: ng.ICompileService
    private pieceFactory: PieceFactory;

    constructor(
        private scope: ICellScope,
        private element: ng.IAugmentedJQuery,
        private attrs: ICellAttrs,
        private $compile: ng.ICompileService,
        private PieceFactory: PieceFactory
    ) {
        this.compile = $compile;
        this.pieceFactory = PieceFactory;

        this.file = parseInt(attrs.file);
        this.rank = parseInt(attrs.rank);

        this.registerWatchPieces();
        this.registerWatchHovered();
        this.registerDragOver();
        this.registerDragEnter();
        this.registerDragLeave();
        this.registerDrop();
    }

    private registerWatchPieces(): void {
        this.scope.$watchCollection(
            'gc.pieces',
            (newValue: Piece[], oldValue, scope) => {
                this.element.removeClass('occupied');
                let found: Piece[] = newValue.filter(
                    (obj, i, arr) => {
                        if (obj.file === this.file && obj.rank === this.rank)
                            return obj;    
                    }
                )

                if (found.length === 1) {
                    this.element.addClass('occupied');
                    this.element.empty();
                    let template = this.pieceFactory.create(found[0]);
                    this.element.append(this.compile(template)(this.scope));
                } else
                    this.element.empty();
            }
        );
    }

    private registerWatchHovered(): void {
        this.scope.$watch('gc.hovered',
            (newValue, oldValue, scope) => {
                if (newValue === null)
                    this.element.removeClass('allow-drop');    
                if (this.scope.gc.isDropAllowed(this.file, this.rank, false))
                    this.element.addClass('allow-drop');
                else
                    this.element.removeClass('allow-drop');
            }, true
        );
    }

    private registerDragOver(): void {
        this.element.on('dragover',
            (evt) => {
                if (this.scope.gc.isDropAllowed(this.file, this.rank))
                    evt.preventDefault();
            }
        );
    }

    private registerDragEnter(): void {
        this.element.on('dragenter',
            (evt) => {
                if (this.scope.gc.isDropAllowed(this.file, this.rank))
                    this.element.addClass('checked-drop');
            }
        );
    }

    private registerDragLeave(): void {
        this.element.on('dragleave',
            (evt) => {
                this.element.removeClass('checked-drop');
            }
        );
    }

    private registerDrop(): void {
        this.element.on('drop',
            (evt) => {
                this.scope.gc.checkDrop(this.file, this.rank).then(
                    (res) => {
                        switch (res.data.message) {
                            case VALIDATION_RESPONSE.CONFIRM:
                            case VALIDATION_RESPONSE.CONFIRM_AS_WIN:
                                this.element.removeClass('allow-drop');
                                this.element.removeClass('checked-drop');
                                this.element.empty();
                                this.element.append(this.scope.gc.draggedElement);
                                this.scope.gc.resolveConfirmMove(res.data.message);
                                break;
                            case VALIDATION_RESPONSE.DENY:
                                console.error(res.data.message);
                                break;
                        }
                    },
                    (err) => {
                        console.error(err);
                    }
                );
            }
        );
    }
}

export default function CellDirective($compile: ng.ICompileService, PieceFactory: PieceFactory) {
    return {
        link: (scope: ICellScope, element: ng.IAugmentedJQuery, attrs: ICellAttrs): Link => new Link(scope, element, attrs, $compile, PieceFactory)
    }
}

CellDirective.$inject = ['$compile', 'PieceFactory'];