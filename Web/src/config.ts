import {
    IStateProvider,
    IUrlRouterProvider
} from 'angular-ui-router';

import {
    RootRoute,
    ROOT_ROUTE
} from './models/routes/browse/Root';
import {
    HomeRoute,
    HOME_ROUTE
} from './models/routes/browse/Home';
import {
    LoginRoute,
    LOGIN_ROUTE
} from './models/routes/browse/Login';
import {
    RegisterRoute,
    REGISTER_ROUTE
} from './models/routes/browse/Register';
import {
    GamesRoute,
    GAMES_ROUTE
} from './models/routes/browse/Games';
import {
    GameRoute,
    GAME_ROUTE
} from './models/routes/browse/Game';

const config = (
    $stateProvider: IStateProvider,
    $urlRouterProvider: IUrlRouterProvider
) => {
    $stateProvider
        .state(ROOT_ROUTE, new RootRoute())
        .state(HOME_ROUTE, new HomeRoute())
        .state(LOGIN_ROUTE, new LoginRoute())
        .state(REGISTER_ROUTE, new RegisterRoute())
        .state(GAMES_ROUTE, new GamesRoute())
        .state(GAME_ROUTE, new GameRoute());
    
    $urlRouterProvider.otherwise('/');
};

export default config;