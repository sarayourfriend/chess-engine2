using Microsoft.AspNetCore.Mvc;

namespace ChessEngine.Web.Controllers
{
    public class HomeController : Controller
    {
        public HomeController() { }

        [Route("~/")]
        public IActionResult Index()
        {
            return View();
        }
    }
}