using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChessEngine.Web.Models.Api.Games;
using ChessEngine.Web.Models.Context;
using ChessEngine.Web.Models.EngineAi;
using Engine.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ChessEngine.Web.Controllers
{
    [Authorize]
    public class GamesController : Controller
    {
        private readonly EngineContext _context;
        private readonly UserManager<User> _userManager;

        public GamesController(EngineContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        [Route("~/api/games/{name}")]
        [HttpGet]
        public async Task<IActionResult> Get(string name)
        {
            var game = await _context.Games.FirstOrDefaultAsync(g => g.Name == name);
            if (game == null)
                return NotFound(name);

            var gameViewModel = new GameViewModel()
            {
                Name = game.Name,
                StartDate = game.StartDate,
                EndDate = game.EndDate
            };

            gameViewModel.LightUserName = game.LightUserId != null ? (await _userManager.FindByIdAsync(game.LightUserId)).UserName : "Unmatched";
            gameViewModel.DarkUserName = game.DarkUserId != null ? (await _userManager.FindByIdAsync(game.DarkUserId)).UserName : "Unmatched";

            return Ok(gameViewModel);
        }

        [Route("~/api/games/{name}/board")]
        [HttpGet]
        public async Task<IActionResult> State(string name)
        {
            var game = await _context.Games.FirstOrDefaultAsync(g => g.Name == name);
            if (game == null)
                return NotFound(name);

            var moveRecords = await _context.MoveRecords.Where(r => r.GameId == game.Id)
                                                        .ToListAsync();


            var moves = new List<Engine.Models.MoveRecord>();

            foreach (var move in moveRecords)
            {
                moves.Add(new Engine.Models.MoveRecord()
                {
                    Id = move.Id,
                    FromFile = move.FromFile,
                    FromRank = move.FromRank,
                    ToFile = move.ToFile,
                    ToRank = move.ToRank
                });
            }

            var board = Board.Build(moves);

            var pieces = new List<PieceViewModel>();

            foreach (var cell in board.Cells)
            {
                if (cell.Piece != null)
                {
                    var piece = cell.Piece;
                    pieces.Add(new PieceViewModel()
                    {
                        Type = piece.Type.ToString(),
                        Color = piece.Color.ToString(),
                        File = piece.X,
                        Rank = piece.Y
                    });
                }
            }

            return Ok(pieces);
        }

        [Route("~/api/games/{name}/history")]
        [HttpGet]
        public async Task<IActionResult> History(string name)
        {
            var user = await _userManager.GetUserAsync(User);
            var game = await _context.Games.FirstOrDefaultAsync(g => g.Name == name);
            if (game == null)
                return NotFound(name);

            if (!(game.LightUserId == user.Id ||
                  game.DarkUserId == user.Id))
                return Unauthorized();

            var moveRecords = await _context.MoveRecords.Where(r => r.GameId == game.Id)
                                                        .OrderByDescending(r => r.Id)
                                                        .ToListAsync();

            var history = new List<HumanReadableMoveViewModel>();

            var translator = new ChessMoveTranslator();

            for (int m = 0; m < moveRecords.Count(); m++)
            {
                var move = moveRecords[m];
                history.Add(new HumanReadableMoveViewModel()
                {
                    Id = m,
                    Value = translator.Translate(move.FromFile, move.FromRank, move.ToFile, move.ToRank)
                });
            }

            var moveRecordsForBoard = new List<Engine.Models.MoveRecord>();

            foreach (var move in moveRecords)
            {
                moveRecordsForBoard.Add(new Engine.Models.MoveRecord()
                {
                    Id = move.Id,
                    FromFile = move.FromFile,
                    FromRank = move.FromRank,
                    ToFile = move.ToFile,
                    ToRank = move.ToRank
                });
            }

            var upToPrevious = moveRecordsForBoard.OrderBy(m => m.Id)
                                                  .Take(moveRecordsForBoard.Count() - 1)
                                                  .ToList();

            var board = Board.Build(upToPrevious);

            var pieces = new List<PieceViewModel>();

            foreach (var cell in board.Cells)
            {
                if (cell.Piece != null)
                {
                    var piece = cell.Piece;
                    pieces.Add(new PieceViewModel()
                    {
                        Color = piece.Color.ToString(),
                        Type = piece.Type.ToString(),
                        File = piece.X,
                        Rank = piece.Y
                    });
                }
            }

            var boardHistoryViewModel = new BoardHistoryViewModel()
            {
                History = history,
                Pieces = pieces
            };

            return Ok(boardHistoryViewModel);
        }

        [Route("~/api/games/{name}/moves")]
        [HttpGet]
        public async Task<IActionResult> Moves(string name)
        {
            var user = await _userManager.GetUserAsync(User);
            var game = await _context.Games.FirstOrDefaultAsync(g => g.Name == name);
            if (game == null)
                return NotFound(name);

            if (!(game.LightUserId == user.Id ||
                  game.DarkUserId == user.Id))
                return Unauthorized();

            if (_context.WinRecords.Any(w => w.GameId == game.Id))
                return Ok(new List<string>() { TurnResponse.GAMEOVER });

            var moveRecords = await _context.MoveRecords.Where(r => r.GameId == game.Id)
                                                        .OrderBy(r => r.Id)
                                                        .ToListAsync();

            if (game.LightUserId == user.Id)
            {
                if (moveRecords.Count % 2 != 0)
                {
                    return Ok(new List<string>() { "No moves" });
                }
                else
                {
                    var moveRecordsForBoard = new List<Engine.Models.MoveRecord>();
                    foreach (var move in moveRecords)
                    {
                        moveRecordsForBoard.Add(new Engine.Models.MoveRecord()
                        {
                            Id = move.Id,
                            FromFile = move.FromFile,
                            FromRank = move.FromRank,
                            ToFile = move.ToFile,
                            ToRank = move.ToRank
                        });
                    }

                    var board = Board.Build(moveRecordsForBoard);
                    var allowedMoves = board.AllMoves(PieceColor.Light);
                    var moves = new List<MoveViewModel>();
                    foreach (var move in allowedMoves)
                    {
                        moves.Add(new MoveViewModel()
                        {
                            FromFile = move.Piece.X,
                            FromRank = move.Piece.Y,
                            ToFile = move.ToX,
                            ToRank = move.ToY
                        });
                    }
                    return Ok(moves);
                }
            }
            else
            {
                if (moveRecords.Count() % 2 == 0)
                {
                    return Ok(new List<string>() { "No moves" });
                }
                else
                {
                    var moveRecordsForBoard = new List<Engine.Models.MoveRecord>();
                    foreach (var move in moveRecords)
                    {
                        moveRecordsForBoard.Add(new Engine.Models.MoveRecord()
                        {
                            Id = move.Id,
                            FromFile = move.FromFile,
                            FromRank = move.FromRank,
                            ToFile = move.ToFile,
                            ToRank = move.ToRank
                        });
                    }

                    var board = Board.Build(moveRecordsForBoard);
                    var allowedMoves = board.AllMoves(PieceColor.Dark);
                    var moves = new List<MoveViewModel>();
                    foreach (var move in allowedMoves)
                    {
                        moves.Add(new MoveViewModel()
                        {
                            FromFile = move.Piece.X,
                            FromRank = move.Piece.Y,
                            ToFile = move.ToX,
                            ToRank = move.ToY
                        });
                    }
                    return Ok(moves);
                }
            }
        }

        [Route("~/api/games/{name}/playerturn")]
        [HttpGet]
        public async Task<IActionResult> PlayerTurn(string name)
        {
            var userId = _userManager.GetUserId(User);
            var game = await _context.Games.FirstOrDefaultAsync(g => g.Name == name);
            if (game == null)
                return NotFound(name);

            if (!(game.DarkUserId != userId ||
                  game.LightUserId != userId))
                return Unauthorized();

            if (game.EndDate != null)
                return Ok(TurnResponse.GAMEOVER);

            var moveCount = (await _context.MoveRecords.Where(m => m.GameId == game.Id)
                                                       .ToListAsync())
                                                       .Count;

            if ((moveCount % 2 == 0 && game.LightUserId == userId) ||
                (moveCount % 2 == 1 && game.DarkUserId == userId))
                return Ok(TurnResponse.CONFIRM);
            else
                return Ok(TurnResponse.DENY);
        }

        [Route("~/api/games/{name}/winner")]
        [HttpGet]
        public async Task<IActionResult> Winner(string name)
        {
            var userId = _userManager.GetUserId(User);
            var game = await _context.Games.FirstOrDefaultAsync(g => g.Name == name);
            if (game == null)
                return NotFound(name);

            if (!(game.LightUserId == userId &&
                  game.DarkUserId == userId))
                return Unauthorized();

            var winRecord = await _context.WinRecords.Include(r => r.User)
                                                     .FirstOrDefaultAsync(r => r.GameId == game.Id);
            if (winRecord == null)
                return NotFound(game.Id);

            return Ok(new WinRecordViewModel()
            {
                GameName = name,
                UserName = winRecord.User.UserName,
                StartDate = game.StartDate,
                EndDate = game.EndDate
            });
        }

        [Route("~/api/games/unmatched")]
        [HttpGet]
        public async Task<IActionResult> Unmatched()
        {
            var userId = _userManager.GetUserId(User);
            var games = await _context.Games.Where(g => g.LightUserId == null ||
                                                        g.DarkUserId == null)
                                            .ToListAsync();
            games.RemoveAll(g => g.LightUserId != null && g.LightUserId == userId ||
                                 g.DarkUserId != null && g.DarkUserId == userId);

            var gameViewModels = new List<GameViewModel>();

            foreach (var game in games)
            {
                var gameViewModel = new GameViewModel()
                {
                    Name = game.Name,
                    StartDate = game.StartDate
                };

                gameViewModel.LightUserName = game.LightUserId != null ? (await _userManager.FindByIdAsync(game.LightUserId)).UserName : "Unmatched";
                gameViewModel.DarkUserName = game.DarkUserId != null ? (await _userManager.FindByIdAsync(game.DarkUserId)).UserName : "Unmatched";

                gameViewModels.Add(gameViewModel);
            }

            return Ok(gameViewModels);
        }

        [Route("~/api/games/{name}")]
        [HttpPost]
        public async Task<IActionResult> Post([FromRoute]string name)
        {
            var user = await _userManager.GetUserAsync(User);

            if (_context.Games.Any(g => g.Name == name))
            {
                ModelState.AddModelError("name", "Name already exists. ");
                return BadRequest(ModelState);
            }

            var game = new Game()
            {
                Name = name,
                StartDate = DateTime.UtcNow,
                LightUser = user
            };

            await _context.Games.AddAsync(game);
            await _context.SaveChangesAsync();

            return Ok(new GameViewModel()
            {
                Name = game.Name,
                StartDate = game.StartDate,
                LightUserName = user.UserName
            });
        }

        [Route("~/api/games/{name}/ai")]
        [HttpPost]
        public async Task<IActionResult> CreateAi([FromRoute]string name)
        {
            var user = await _userManager.GetUserAsync(User);
            var aiUser = await _userManager.FindByNameAsync(Startup.CHESS_ENGINE_USER);

            if (_context.Games.Any(g => g.Name == name))
            {
                ModelState.AddModelError("name", "Name already exists. ");
                return BadRequest(ModelState);
            }

            var game = new Game()
            {
                Name = name,
                StartDate = DateTime.UtcNow,
                LightUser = user,
                DarkUser = aiUser
            };

            await _context.Games.AddAsync(game);
            await _context.SaveChangesAsync();

            return Ok(new GameViewModel()
            {
                Name = game.Name,
                StartDate = game.StartDate,
                LightUserName = user.UserName,
                DarkUserName = aiUser.UserName
            });
        }

        [Route("~/api/games/{name}/moves")]
        [HttpPost]
        public async Task<IActionResult> MakeMove(string name, [FromBody]MoveRequest model)
        {
            var userId = _userManager.GetUserId(User);
            var game = await _context.Games.FirstOrDefaultAsync(g => g.Name == name);
            if (game == null)
                return NotFound(name);

            if (!(game.LightUserId == userId ||
                  game.DarkUserId == userId))
                return Unauthorized();

            var moveRecords = await _context.MoveRecords.Where(r => r.GameId == game.Id)
                                                        .ToListAsync();
            var moveRecordsForBoard = new List<Engine.Models.MoveRecord>();
            foreach (var move in moveRecords)
            {
                moveRecordsForBoard.Add(new Engine.Models.MoveRecord()
                {
                    Id = move.Id,
                    FromFile = move.FromFile,
                    FromRank = move.FromRank,
                    ToFile = move.ToFile,
                    ToRank = move.ToRank
                });
            }

            var board = Board.Build(moveRecordsForBoard);
            var moveRecordForBoard = new Engine.Models.MoveRecord()
            {
                FromFile = model.FromFile,
                FromRank = model.FromRank,
                ToFile = model.ToFile,
                ToRank = model.ToRank
            };

            var validationResult = board.ValidateMove(moveRecordForBoard);

            if (validationResult == MoveValidationResult.CONFIRM ||
                validationResult == MoveValidationResult.CONFIRM_AS_WIN)
            {
                _context.MoveRecords.Add(new Models.Context.MoveRecord()
                {
                    GameId = game.Id,
                    FromFile = model.FromFile,
                    FromRank = model.FromRank,
                    ToFile = model.ToFile,
                    ToRank = model.ToRank
                });

                if (validationResult == MoveValidationResult.CONFIRM_AS_WIN)
                {
                    DateTime endDate;
                    game.EndDate = endDate = DateTime.UtcNow;

                    var winRecord = new WinRecord()
                    {
                        Game = game
                    };

                    winRecord.User = await _userManager.FindByIdAsync(userId);

                    _context.WinRecords.Add(winRecord);

                    await _context.SaveChangesAsync();
                    return Ok(new MoveValidationViewModel()
                    {
                        Message = MoveValidationResult.CONFIRM_AS_WIN,
                        Move = model,
                        WinRecord = new WinRecordViewModel()
                        {
                            GameName = game.Name,
                            UserName = winRecord.User.UserName,
                            StartDate = game.StartDate,
                            EndDate = endDate
                        }
                    });
                }

                await _context.SaveChangesAsync();

                var aiId = (await _userManager.FindByNameAsync(Startup.CHESS_ENGINE_USER)).Id;
                if (game.DarkUserId == aiId || game.LightUserId == aiId)
                {
                    var ai = new Ai();
                    await ai.Decide(_context, game.Id);
                }

                return Ok(new MoveValidationViewModel()
                {
                    Message = MoveValidationResult.CONFIRM,
                    Move = model
                });
            }
            else
            {
                return Ok(new MoveValidationViewModel()
                {
                    Message = MoveValidationResult.DENY,
                    Move = model
                });
            }
        }

        [Route("~/api/games/{name}/ai/GO")]
        [HttpPut]
        public async Task<IActionResult> AiGo([FromRoute]string name)
        {
            var game = await _context.Games.FirstOrDefaultAsync(g => g.Name == name);
            if (game == null)
                return NotFound(name);

            var aiId = (await _userManager.FindByNameAsync(Startup.CHESS_ENGINE_USER)).Id;
            if (game.DarkUserId == aiId || game.LightUserId == aiId)
            {
                var ai = new Ai();
                await ai.Decide(_context, game.Id);
            }

            return Ok();
        }

        [Route("~/api/games/{name}")]
        [HttpPut]
        public async Task<IActionResult> Join([FromRoute]string name)
        {
            var userId = _userManager.GetUserId(User);
            var game = await _context.Games.FirstOrDefaultAsync(g => g.Name == name);
            if (game == null)
                return NotFound(name);

            var gameViewModel = new GameViewModel()
            {
                Name = game.Name,
                StartDate = game.StartDate,
                EndDate = game.EndDate
            };

            if (game.LightUserId == userId || game.DarkUserId == userId)
            {
                gameViewModel.LightUserName = game.LightUserId != null ? (await _userManager.FindByIdAsync(game.LightUserId)).UserName : "Unmatched";
                gameViewModel.DarkUserName = game.DarkUserId != null ? (await _userManager.FindByIdAsync(game.DarkUserId)).UserName : "Unmatched";

                return Ok(gameViewModel);
            }
            else if (game.LightUserId == null)
                game.LightUserId = userId;
            else if (game.DarkUserId == null)
                game.DarkUserId = userId;
            else
                return BadRequest("Game already in progress.");

            await _context.SaveChangesAsync();

            gameViewModel.LightUserName = (await _userManager.FindByIdAsync(game.LightUserId)).UserName;
            gameViewModel.DarkUserName = (await _userManager.FindByIdAsync(game.DarkUserId)).UserName;

            return Ok(gameViewModel);
        }
    }
}