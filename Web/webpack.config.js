var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var isProd = process.env.NODE_ENV === 'production';

module.exports = {
    entry: [
        './src/app.ts',
        './src/css/styles.scss'
    ],
    output: {
        filename: 'js/bundle.js',
        path: path.resolve(__dirname, 'wwwroot'),
        publicPath: isProd ? '/' : 'http://localhost:7000/dist/',
    },
    module: {
        rules: [
            { test: /\.tsx?$/, loader: 'ts-loader', exclude: /node_modules/ },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader!resolve-url-loader!sass-loader' }),
            },
            { test: /\.html$/, loader: 'raw-loader' },
            { test: /\.svg$/, loader: 'svg-url-loader' },
        ],
    },
    resolve: {
        extensions: [".ts", ".js"],
    },
    devtool: isProd ? false : 'source-map',
    devServer: {
        port: 7000,
    },
    plugins: [
        new ExtractTextPlugin({ filename: './css/styles.css', allChunks: true }),
    ]
};