using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChessEngine.Web.Models.Context;
using Engine;
using Engine.Models;
using Microsoft.EntityFrameworkCore;

namespace ChessEngine.Web.Models.EngineAi
{
    public class Ai
    {
        public async Task<bool> Decide(EngineContext context, int gameId)
        {
            var game = await context.Games.Include(g => g.DarkUser)
                                        .Include(g => g.LightUser)
                                        .FirstOrDefaultAsync(g => g.Id == gameId);
            if (game == null)
                return false;

            if (!(game.DarkUser.UserName == Startup.CHESS_ENGINE_USER ||
                game.LightUser.UserName == Startup.CHESS_ENGINE_USER))
                return false;

            var moveRecords = await context.MoveRecords.Where(r => r.GameId == gameId)
                                                    .ToListAsync();

            if (moveRecords.Count() % 2 != 1)
                return false;

            var moves = new List<Engine.Models.MoveRecord>();

            foreach (var move in moveRecords)
            {
                moves.Add(new Engine.Models.MoveRecord()
                {
                    Id = move.Id,
                    FromFile = move.FromFile,
                    FromRank = move.FromRank,
                    ToFile = move.ToFile,
                    ToRank = move.ToRank
                });
            }

            var board = Board.Build(moves);

            var artificial = new Artificial(board, PieceColor.Dark);
            var nextMove = artificial.highest.Parents.FirstOrDefault().ThisMove;

            if (nextMove == null)
                return false;

            var moveRecordForBoard = new Engine.Models.MoveRecord()
            {
                FromFile = nextMove.FromFile,
                FromRank = nextMove.FromRank,
                ToFile = nextMove.ToFile,
                ToRank = nextMove.ToRank
            };

            board = Board.Build(moves);

            var validationResult = board.ValidateMove(moveRecordForBoard);

            if (validationResult == MoveValidationResult.CONFIRM ||
                validationResult == MoveValidationResult.CONFIRM_AS_WIN)
            {
                context.MoveRecords.Add(new Models.Context.MoveRecord()
                {
                    GameId = game.Id,
                    FromFile = nextMove.FromFile,
                    FromRank = nextMove.FromRank,
                    ToFile = nextMove.ToFile,
                    ToRank = nextMove.ToRank
                });

                if (validationResult == MoveValidationResult.CONFIRM_AS_WIN)
                {
                    DateTime endDate;
                    game.EndDate = endDate = DateTime.UtcNow;

                    var winRecord = new WinRecord()
                    {
                        Game = game
                    };

                    winRecord.User = (User)(await context.Users.FirstOrDefaultAsync(u => u.UserName == Startup.CHESS_ENGINE_USER));

                    await context.WinRecords.AddAsync(winRecord);
                }
            }

            await context.SaveChangesAsync();

            return true;
        }
    }
}