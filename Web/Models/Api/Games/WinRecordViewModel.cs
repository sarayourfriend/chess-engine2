using System;

namespace ChessEngine.Web.Models.Api.Games
{
    public class WinRecordViewModel
    {
        public string GameName { get; set; }
        public string UserName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}