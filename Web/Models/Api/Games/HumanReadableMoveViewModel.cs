namespace ChessEngine.Web.Models.Api.Games
{
    public class HumanReadableMoveViewModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}