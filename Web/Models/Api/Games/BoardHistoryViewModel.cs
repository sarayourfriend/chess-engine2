using System.Collections.Generic;

namespace ChessEngine.Web.Models.Api.Games
{
    public class BoardHistoryViewModel
    {
        public List<HumanReadableMoveViewModel> History { get; set; } = new List<HumanReadableMoveViewModel>();
        public List<PieceViewModel> Pieces { get; set; } = new List<PieceViewModel>();
    }
}