namespace ChessEngine.Web.Models.Api.Games
{
    public class MoveValidationViewModel
    {
        public string Message { get; set; }
        public MoveRequest Move { get; set; }
        public WinRecordViewModel WinRecord { get; set; }
    }
}