using System;

namespace ChessEngine.Web.Models.Api.Games
{
    public class GameViewModel
    {
        public string Name { get; set; }
        public string LightUserName { get; set; }
        public string DarkUserName { get; set; }
        public string WinnerUserName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}