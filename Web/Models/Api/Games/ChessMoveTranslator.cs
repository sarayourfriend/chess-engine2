namespace ChessEngine.Web.Models.Api.Games
{
    public class ChessMoveTranslator
    {
        public string Translate(int fromFile, int fromRank, int toFile, int toRank)
        {
            return string.Format(
                "{0}{1}{2}{3}", 
                this.ParseFile(fromFile), 
                this.ParseRank(fromRank), 
                this.ParseFile(toFile), 
                this.ParseRank(toRank));
        }

        internal char ParseFile(int input)
        {
            switch (input)
            {
                default:
                case 0:
                    return 'A';
                case 1:
                    return 'B';
                case 2:
                    return 'C';
                case 3:
                    return 'D';
                case 4:
                    return 'E';
                case 5:
                    return 'F';
                case 6:
                    return 'G';
                case 7:
                    return 'H';
            }
        }

        internal int ParseRank(int input)
        {
            return 8 - input;
        }
    }
}