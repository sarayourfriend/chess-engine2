using System.Collections.Generic;
using ChessEngine.Web.Models.Api.Games;

namespace ChessEngine.Web.Models.Api.Accounts
{
    public class UserViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public List<GameViewModel> Games { get; set; } = new List<GameViewModel>();
    }
}