namespace ChessEngine.Web.Models.Context
{
    public class WinRecord
    {
        public int Id { get; set; }
        public int GameId { get; set; }
        public string UserId { get; set; }

        public Game Game { get; set; }
        public User User { get; set; }
    }
}