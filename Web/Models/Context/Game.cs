using System;
using System.Collections.Generic;

namespace ChessEngine.Web.Models.Context
{
    public class Game
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LightUserId { get; set; }
        public string DarkUserId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public ICollection<MoveRecord> Moves { get; set; }
        public User LightUser { get; set; }
        public User DarkUser { get; set; }
    }
}