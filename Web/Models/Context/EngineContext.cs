using ChessEngine.Web.Models.EngineAi;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ChessEngine.Web.Models.Context
{
    public class EngineContext : IdentityDbContext
    {
        public DbSet<Game> Games { get; set; }
        public DbSet<MoveRecord> MoveRecords { get; set; }
        public DbSet<WinRecord> WinRecords { get; set; }

        public EngineContext(DbContextOptions<EngineContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<User>(user =>
            {
                user.ForSqliteToTable("Users");

                user.HasMany(u => u.Wins)
                    .WithOne(w => w.User)
                    .HasForeignKey(w => w.UserId);
            });

            builder.Entity<IdentityRole>()
                .ForSqliteToTable("Roles");
            builder.Entity<IdentityUserRole<string>>()
                .ForSqliteToTable("UserRoles");
            builder.Entity<IdentityRoleClaim<string>>()
                .ForSqliteToTable("UserClaims");
            builder.Entity<IdentityUserLogin<string>>()
                .ForSqliteToTable("UserLogins");
            builder.Entity<IdentityUserToken<string>>()
                .ForSqliteToTable("UserTokens");

            builder.Entity<Game>(game => 
            {
                game.HasOne(g => g.LightUser)
                    .WithMany(u => u.LightGames)
                    .HasForeignKey(g => g.LightUserId);

                game.HasOne(g => g.DarkUser)
                    .WithMany(u => u.DarkGames)
                    .HasForeignKey(g => g.DarkUserId);

                game.HasIndex(g => g.Name)
                    .IsUnique(true);
            });

            builder.Entity<WinRecord>()
                .HasOne(w => w.User)
                .WithMany(u => u.Wins)
                .HasForeignKey(w => w.UserId)
                .IsRequired(true);
        }
    }
}